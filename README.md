                                              **News app task**
## Description

News app contain home screen that has list of articles , nave drawer and news details.


## technologies i used

1. Kotlin with Rxjava.
2. MVVM design pattern.
3. Live data and observer pattern.
4. Retrofit for remote data.
5. Nave Graph with Drawer navigation.
6. used Observable.zip to concat 2 list .
7. used extension functions.
8. Caching by OkHttpClient.
9. Use Repository design pattern.
10. View binding.


## Installation

download app from media fire link and install it.

