package com.link.newsfeedapplink.ui.home

import com.link.newsfeedapplink.models.NewsFeedsModel
import com.link.newsfeedapplink.network.RetrofitClient
import io.reactivex.Observable

class HomeRepository {
    fun getNews(source :String,apiKey:String ):  Observable<NewsFeedsModel> {
        return RetrofitClient.getInstance().api.getNewsFeeds(source,apiKey)
    }
}