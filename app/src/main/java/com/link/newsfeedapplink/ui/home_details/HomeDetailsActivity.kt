package com.link.newsfeedapplink.ui.home_details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.link.newsfeedapplink.R
import com.link.newsfeedapplink.databinding.ActivityHomeDetailsBinding
import com.link.newsfeedapplink.databinding.ActivityMainBinding
import com.link.newsfeedapplink.utils.loadImage

class HomeDetailsActivity : AppCompatActivity() {
    private var _binding: ActivityHomeDetailsBinding? = null
    private val binding get() = _binding!!

    var image: String? = null
    var date: String? = null
    var description: String? = null
    var title: String? = null
    var author: String? = null
    var url: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityHomeDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intent = intent
        image = intent.getStringExtra("image")
        author = intent.getStringExtra("author")
        date = intent.getStringExtra("date")
        description = intent.getStringExtra("description")
        title = intent.getStringExtra("title")
        url = intent.getStringExtra("url")

        binding.newsAuthor.text = author
        binding.newsDate.text = date
        binding.newsDescription.text = description
        binding.newsTitle.text = title
        image?.let { binding.newsImage.loadImage(it) }
        binding.openWebsite.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }

        binding.back.setOnClickListener {
            finish()
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()

    }
}