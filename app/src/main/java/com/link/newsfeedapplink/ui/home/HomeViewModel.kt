package com.link.newsfeedapplink.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.link.newsfeedapplink.models.Articles
import com.link.newsfeedapplink.models.NewsFeedsModel
import com.link.newsfeedapplink.utils.Constants.API_KEY
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class HomeViewModel() : ViewModel() {

    private var newsFeedsMutableLiveData= MutableLiveData<List<Articles>>()
    private val repository = HomeRepository()

    fun getNewsFeeds(): LiveData<List<Articles>> {
            newsFeedsMutableLiveData = MutableLiveData()
            loadNewsFeeds()

        return newsFeedsMutableLiveData
    }

    private fun loadNewsFeeds() {
        Observable.zip(repository.getNews( "the-next-web", API_KEY),
            repository.getNews( "associated-press", API_KEY),
            { nextWeb: NewsFeedsModel, associatedPress: NewsFeedsModel ->
                val articlesList: MutableList<Articles> =
                    ArrayList()
                articlesList.addAll(nextWeb.articles)
                articlesList.addAll(associatedPress.articles)
                articlesList
            }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<Articles?>> {
                override fun onSubscribe(d: Disposable) {}
                override fun onNext(articles: List<Articles?>) {
                    newsFeedsMutableLiveData.value = articles as List<Articles>
                }

                override fun onError(e: Throwable) {
                }
                override fun onComplete() {}
            })
    }
}