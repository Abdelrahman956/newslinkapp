package com.link.newsfeedapplink.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.link.newsfeedapplink.R
import com.link.newsfeedapplink.databinding.FragmentHomeBinding
import com.link.newsfeedapplink.utils.Constants.TAG

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        homeViewModel.getNewsFeeds().observe(viewLifecycleOwner) { newsFeedsModel ->

            val adapter = NewsFeedsAdapter(requireContext(), newsFeedsModel)
            binding.newsFeedsRecycler.adapter = adapter
            Log.e(TAG, "" + Gson().toJson(newsFeedsModel))

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}