package com.link.newsfeedapplink.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.link.newsfeedapplink.databinding.NewsFeedsRowBinding
import com.link.newsfeedapplink.models.Articles
import com.link.newsfeedapplink.ui.home_details.HomeDetailsActivity
import com.link.newsfeedapplink.utils.formatDate
import com.link.newsfeedapplink.utils.loadImage


class NewsFeedsAdapter(var context: Context, var newsFeedsModel: List<Articles>) :
    RecyclerView.Adapter<NewsFeedsAdapter.NewsFeedsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedsViewHolder {
        return NewsFeedsViewHolder(
            NewsFeedsRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NewsFeedsViewHolder, position: Int) {

        holder.binding.newsTitle.text = newsFeedsModel[position].title

        holder.binding.newsAuthor.text = "By " + newsFeedsModel[position].author

        holder.binding.newsDate.text = formatDate(newsFeedsModel[position].publishedAt)

        newsFeedsModel[position].urlToImage?.let { holder.binding.newsImage.loadImage(it) }

        holder.itemView.setOnClickListener {
            val intent = Intent(context, HomeDetailsActivity::class.java)
            intent.putExtra("image", newsFeedsModel[position].urlToImage)
            intent.putExtra("author", "By " + newsFeedsModel[position].author)
            intent.putExtra("title", newsFeedsModel[position].title)
            intent.putExtra("description", newsFeedsModel[position].description)
            intent.putExtra("date", formatDate(newsFeedsModel[position].publishedAt))
            intent.putExtra("url", newsFeedsModel[position].url)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return newsFeedsModel.size
    }

    inner class NewsFeedsViewHolder(var binding: NewsFeedsRowBinding) :
        RecyclerView.ViewHolder(binding.root)

}