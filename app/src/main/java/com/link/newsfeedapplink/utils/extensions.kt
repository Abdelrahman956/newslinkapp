package com.link.newsfeedapplink.utils

import android.text.format.DateFormat
import android.widget.ImageView

import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*


fun ImageView.loadImage(url: String) {
    Glide.with(this).load(url).into(this)
}

fun formatDate(createdAt: String?): String? {
    if (createdAt == null || createdAt.isEmpty()) return ""
    val fmt =
        SimpleDateFormat("yyyy-MM-dd", Locale.US)
    var date: Date? = null
    try {
        date = fmt.parse(createdAt)
        val fmtOut =
            SimpleDateFormat("dd-MM-yyyy", Locale.US)

        val dayOfTheWeek =
            DateFormat.format("EEEE", date) as String // Thursday


        return dayOfTheWeek + " " + fmtOut.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return ""
}



