package com.link.newsfeedapplink

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.navigation.NavigationView
import com.link.newsfeedapplink.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private  var _binding: ActivityMainBinding? = null
    val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        setSupportActionBar(binding.appbarLayout.toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        binding.navView.setNavigationItemSelectedListener(this)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
            ), binding.drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        //navView.setupWithNavController(navController)
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.explore -> {
                Toast.makeText(this, getString(R.string.explore), Toast.LENGTH_SHORT)
                    .show()
            }
            R.id.live_chat -> {
                Toast.makeText(
                    this,
                    getString(R.string.live_chat),
                    Toast.LENGTH_SHORT
                ).show()
            }
            R.id.gallery -> {
                Toast.makeText(this, getString(R.string.gallery), Toast.LENGTH_SHORT)
                    .show()
            }
            R.id.wish_list -> {
                Toast.makeText(
                    this,
                    getString(R.string.wish_list),
                    Toast.LENGTH_SHORT
                ).show()
            }
            R.id.e_magazine -> {
                Toast.makeText(
                    this,
                    getString(R.string.e_magazine),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }


        // drawer.closeDrawer(GravityCompat.START);
        return true
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()

    }
}