package com.link.newsfeedapplink.models

import com.google.gson.annotations.SerializedName

data class NewsFeedsModel (
    @SerializedName("articles") val articles: List<Articles>,

    @SerializedName("status") val status: String

)

   data class Articles (
        @SerializedName("author")
        val author: String,

        @SerializedName("title")
        val title: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("urlToImage")
        val urlToImage: String,

        @SerializedName("url")
        val url: String,

        @SerializedName("publishedAt")
        val publishedAt: String,
)
