package com.link.newsfeedapplink.network

import com.link.newsfeedapplink.models.NewsFeedsModel
import io.reactivex.Observable

import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("articles")
    fun getNewsFeeds(
        @Query("source") source: String,
        @Query("apiKey") apiKey: String
    ): Observable<NewsFeedsModel>

}


